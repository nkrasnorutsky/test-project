import * as React from 'react';
import { IRecord } from 'src/model/record/record.model';
import { recordService } from 'src/services/RecordService/RecordService';

interface IEditRawProps {
    /**
     * id текущей записи
     */
    currentId: string;
    /**
     * перезагрузит страницу
     */
    reloadPage: () => void;
}

interface IEditRawState {
    record: IRecord | null;
    user_name: string;
    user_email: string;
    user_id: number | null;
    user_age: number | null ;
}

export default class EditRaw extends React.Component<IEditRawProps, IEditRawState> {
    constructor(props: IEditRawProps) {
        super(props);
        this.state = {
            record: null,
            user_name: '',
            user_age: null,
            user_email: '',
            user_id: null
        }

        this.getRecord = this.getRecord.bind(this);
        this.handleChangeUserName = this.handleChangeUserName.bind(this);
        this.sandRecord = this.sandRecord.bind(this);
    }

    public componentDidMount() {
        recordService.fetchOneRecord(this.props.currentId)
            .then(res => {
                this.setState({
                    user_name: res.data.name,
                    user_age: res.data.age,
                    user_email: res.data.email,
                    user_id: res.data.id

                });
            })
        this.getRecord();
    }
    // По-хорошему, можно сделать один управляемый компонент, который будет принимать текущее значение инпута, изменять и сохранять его.
    public handleChangeUserName = (event: React.FormEvent<HTMLInputElement>) => {
        event.preventDefault();
        this.setState({
             user_name: event.currentTarget.value,
        })
    }

    public handleChangeUserEmail = (event: React.FormEvent<HTMLInputElement>) => {
     event.preventDefault();
         this.setState({
              user_email: event.currentTarget.value,
         })
    }

    public handleChangeUserAge = (event: React.FormEvent<HTMLInputElement>) => {
        event.preventDefault();
        this.setState({
             user_age: +event.currentTarget.value,
        })
    }

    public handleChangeUserId = (event: React.FormEvent<HTMLInputElement>) => {
        event.preventDefault();
        this.setState({
             user_id: +event.currentTarget.value,
        })
    }

    public render() {
        const isReady = this.state.record !== null;
        return (
            <div className="table-row-container">
               {isReady ? (this.renderRecord()) : 'err'}
           </div>
        )
    }
    /**
     * Получаем запись
     */
    private getRecord() {
        recordService.fetchOneRecord(this.props.currentId)
            .then(record => {
                this.setState({
                    record
                });
            });
    }
    /**
     * отображение записи с текущими значениями
     */
    private renderRecord(): JSX.Element {
        return (
                <div className="table-row-container">
                    <input className="table-row-element" placeholder="id" type="number" value={this.state.user_id!} onChange={this.handleChangeUserId} />
                    <input className="table-row-element" placeholder="name" type="text" value={this.state.user_name} onChange={this.handleChangeUserName}/>
                    <input className="table-row-element" placeholder="age" type="number" value={this.state.user_age!} onChange={this.handleChangeUserAge} />
                    <input className="table-row-element" placeholder="email" type="text" value={this.state.user_email} onChange={this.handleChangeUserEmail} />
                    <button className="save-button" onClick={() => this.sandRecord()} >Сохранить</button>
                </div>
            )
    }
    /**
     * Отпрака измененной записи на сервер
     */
    private sandRecord() {
        const body = {
            data: {
                id: this.state.user_id,
                name: this.state.user_name,
                email: this.state.user_email,
                age: this.state.user_age
            }
        } as IRecord;

        recordService.updateRecord(this.props.currentId, body)
            .then(() => [recordService.fetchAllRecords(), this.props.reloadPage()])
            .catch(err => console.log(err))
            .finally(() => console.log("sended"))
    }
}