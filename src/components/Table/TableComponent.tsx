import * as React from 'react';
import './TableComponent.css';
import { recordService } from 'src/services/RecordService/RecordService';
import { IRecord } from 'src/model/record/record.model';
import TableRow from '../TableRow/TableRow';
import EditRaw from '../EditComponent/EditComponent';

interface ITableProps {}
interface ITableState {
    /**
     * Список всех записей.
     */
    records: IRecord[] | null;
    /**
     * Показывается ли окно добавления записи.
     */
    isVisible: boolean;
    /**
     * Показывается ли окно редактирования записи
     */
    isEdit: boolean;
    /**
     * Одна запись
     */
    oneRecord: IRecord | null;
    /**
     * Id Текущей записи
     */
    currentId: string;
}

export default class Table extends React.Component<ITableProps, ITableState> {
    constructor(props: ITableProps) {
        super(props);
        this.state = {
            records: [],
            isVisible: false,
            isEdit: false,
            oneRecord: null,
            currentId: ''
        }

        this.fetchRecords = this.fetchRecords.bind(this);
        this.showOrHideTableRow = this.showOrHideTableRow.bind(this);
        this.deleteRecord = this.deleteRecord.bind(this);
        this.reloadForAdd = this.reloadForAdd.bind(this);
        this.reloadForEdit = this.reloadForEdit.bind(this);
        this.editRecord = this.editRecord.bind(this);
        this.showOrHideEditRaw = this.showOrHideEditRaw.bind(this);
    }

    public componentWillMount() {
        this.fetchRecords();
    }

    public render() {
        const isReady = this.state.records !== null;
        return (
            <React.Fragment>
            <div className="table-container">
                <div className="table-header">
                   <div className="table-id">Id</div>
                   <div className="table-header-element">Имя</div>
                   <div className="table-header-element">Возраст</div>
                   <div className="table-header-element">email-адрес</div>
                   <button className="add-button" onClick={this.showOrHideTableRow}>Добавить</button>
                </div>
            </div>
            <div>
                {isReady ? (this.renderRecords()) : 'Отсутствуют записи в базе данных'}
            </div>
            {this.state.isVisible ? <TableRow reloadPage={this.reloadForAdd}/> : ''}
            {this.state.isEdit ? <EditRaw currentId={this.state.currentId} reloadPage={this.reloadForEdit}/> : ''}
            </React.Fragment>
        );
    }
    /**
     * Получение списка всех записей.
     */
    private fetchRecords() {
        recordService.fetchAllRecords()
            .then(records => {
                this.setState({records});
            })
            .catch(error => console.log(error))
            .finally(() => console.log("запрос отработал"))
    }
    /**
     * Отрисовывает список всех записей.
     */
    private renderRecords(): JSX.Element[] {
        return this.state.records!.map((data, key) => {
            
            return (
                <div className="table-container-record" key={key}>
                    <div className="table-header">
                        <div className="table-id">{data.data.id}</div>
                        <div className="table-header-element">{data.data.name}</div>
                        <div className="table-header-element">{data.data.age}</div>
                        <div className="table-header-element">{data.data.email}</div>
                        <button className="edit-button" onClick={() => this.editRecord(data._id)}>Редактировать</button>
                        <button className="delete-button" onClick={() => this.deleteRecord(data._id)} >Удалить</button>
                    </div>
                    
                </div>
            )
        })
    }
    /**
     * Открывает либо закрывает модальное окно.
     */
    private showOrHideTableRow() {
        this.setState({
            isVisible: !this.state.isVisible
        });
    }
    /**
     * Открывает либо закрывает окно редактирования.
     */
    private showOrHideEditRaw() {
        this.setState({
            isEdit: !this.state.isEdit
        });
    }
    /**
     * Удаление записи.
     * Удаляет запись и делает запрос на получение обновленного списка.
     * @param id
     */
    private deleteRecord(id: string) {
        recordService.deleteRecoed(id)
            .then(() => this.fetchRecords())
            .catch(error => console.log(error))
    }
    /**
     * Скрытие модального окна и получение обновленных данных
     */
    private reloadForAdd() {
        this.showOrHideTableRow();
        this.fetchRecords();        
    }
    private reloadForEdit() {
        this.showOrHideEditRaw();
        this.fetchRecords()
    }
    /**
     * Редактирование записи.
     */
    private editRecord(id: string) {
        this.showOrHideEditRaw();
        this.setState({currentId: id});
    }
}