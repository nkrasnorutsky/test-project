import * as React from 'react';
import { recordService } from 'src/services/RecordService/RecordService';
import { IRecord } from 'src/model/record/record.model';
import './TableRow.css';

interface ITableRowProps {
    reloadPage: () => void;
}

export default class TableRow extends React.Component<ITableRowProps> {
    // Переменные класса для работы со значениями инпутов
    private inputId: HTMLInputElement | null = null;
    private inputName: HTMLInputElement | null = null;
    private inputAge: HTMLInputElement | null = null;
    private inputEmail: HTMLInputElement | null = null;
    
    constructor(props: ITableRowProps) {
        super(props);
        this.state = {}

        this.sendRecordData = this.sendRecordData.bind(this);
       
    }

    public render() {
        return (
           <div className="table-row-container">
               <input className="table-row-element" placeholder="id" type="number" ref={input => this.inputId = input} />
               <input className="table-row-element" placeholder="name" type="text" ref={input => this.inputName = input} />
               <input className="table-row-element" placeholder="age" type="number" ref={input => this.inputAge = input} />
               <input className="table-row-element" placeholder="email" type="text" ref={input => this.inputEmail = input} />
               <button className="save-button" onClick={this.sendRecordData} >save</button>
           </div>
        );
    }
    /**
     * Отправляет данные на сервер
     */
    private sendRecordData() {
        const [ userId, userAge ] = [ this.inputId!.value, this.inputAge!.value ];
        const [ userName, userEmail ] = [ this.inputName!.value, this.inputEmail!.value ];
        
        const body = {
            data: {
                id: +userId,
                name: userName,
                age: +userAge,
                email: userEmail
            }
        } as IRecord;

        recordService.sendNewRecoed(body)
            .then(() => [recordService.fetchAllRecords(), this.props.reloadPage()])
            .catch(error => console.log(error))  
        
    }
}