import * as React from 'react';
import Table from 'src/components/Table/TableComponent';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

export default class Root extends React.Component {

    public render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/main-page" exact={true} component={Table} />
                    <Redirect from="/" to="/main-page" />            
                </Switch>
            </BrowserRouter>
        );
    }
    
}