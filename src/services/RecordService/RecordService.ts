import { AbstractNetworkService } from "../AbstractNetworkService/AbstractNetworrkService";
import { IRecord } from 'src/model/record/record.model';

/**
 * Сервис для работы с бэкендом
 */
class RecordService extends AbstractNetworkService {
    private readonly apiUrl = 'http://178.128.196.163:3000/api/';

    /**
     * Получить все записи
     */
    public fetchAllRecords(): Promise<IRecord[]> {
        return this.get(`${this.apiUrl}records`);
    }
    /**
     * Получить одну запись
     * @param id 
     */
    public fetchOneRecord(id: string): Promise<IRecord> {
        return this.get(`${this.apiUrl}records/${id}`);
    }
    /**
     * Обновить запись
     * @param id 
     */
    public updateRecord(id: string, body: any): Promise<IRecord> {
        return this.post(`${this.apiUrl}records/${id}`, body);
    }
    /**
     * Создать новую запись
     * @param body 
     */
    public sendNewRecoed(body: IRecord): Promise<IRecord> {
        return this.put(`${this.apiUrl}records`, body);
    }
    /**
     * Удалить запись
     * @param id 
     */
    public deleteRecoed(id: string) {
        return  this.deleteRequest(`${this.apiUrl}records/${id}`);
    }
}

export const recordService = new RecordService();