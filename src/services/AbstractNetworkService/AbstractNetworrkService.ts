import axios from 'axios';

/**
 * Базовый абстрактный класс. От него должны наследоваться все сервисы.
 */
export class AbstractNetworkService {
    /**
     * Базовая функция для гет-запроса
     * @param url 
     * @param params 
     */
    protected get<T>(url: string, params?: {[key: string]: any}): Promise<T> {
        
        return axios
            .get<T>(url,{
                params,
            })
            .then(response => response.data)
    };
    /**
     * Базовая функция для пост-запроса
     * @param url 
     * @param body 
     * @param params 
     */
    protected post<T>(url: string, body?: {[key: string]: any} | FormData, params?: {[key: string]: any}): Promise<T> {
        
        return axios
            .post<T>(url, body, {
                params
            })
            .then(response => response.data)
    };
    /**
     * Базовая функция для пут-запроса
     * @param url 
     * @param body 
     * @param params 
     */
    protected put<T>(url: string, body?: {[key: string]: any} | FormData, params?: {[key: string] : any}): Promise<T> {

        return axios
            .put<T>(url, body, {
                params
            })
            .then(response => response.data)
    }
    /**
     * Базовая функция для делит-запроса
     * @param url 
     * @param body 
     * @param params 
     */
    protected deleteRequest<T>(url: string, body?: {[key: string]: any} | FormData, params?: {[key: string] : any}): Promise<T> {

        return axios
            .delete<T>(url)
            .then(response => response.data)
            
    }
}