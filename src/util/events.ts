/**
 * Достаёт значение из React.SyntheticEvent и передаёт его функции-аргументу
 * @param callback 
 */
export function fromReactEvent(callback: (value: string, element?: HTMLElement) => void, stopPropagation: boolean = false) {
    // tslint:disable-next-line: only-arrow-functions
    return function(event: React.SyntheticEvent) {
        if (stopPropagation) {
            event.stopPropagation();
        }

        const target = event.target as HTMLElement;
        const value = (target as any).value as string;

        callback(value, target);
    }
}