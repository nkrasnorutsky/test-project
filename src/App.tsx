import * as React from 'react';
import './App.css';

import Root from './components/container/Root/RootComponent';

class App extends React.Component {
  public render() {
    return (
      <Root />
    );
  }
}

export default App;
