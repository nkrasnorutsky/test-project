/**
 * Интерфейс, описывающий запись
 */
export interface IRecord {
    data: {
        /**
         * Айди пользователя
         */
        id: number;
        /**
         * Имя пользователя
         */
        name: string;
        /**
         * Емейл пользователя
         */
        email: string;
        /**
         * Возраст пользователя
         */
        age: number;
    }
    /**
     * id записи
     */
    _id: string;
}